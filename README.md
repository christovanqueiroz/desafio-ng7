# Dashboard NG7

## Instruções

Para rodar o projeto, é necessário digitar "npm install" no terminal do seu IDE (editor de códigos), e após a instalação dos arquivos, abrir com a ferramenta Live Server.

## Funcionalidades

É possível verificar cada tipo de dado ao selecionar o input desejado na página, conforme exemplo abaixo.

![Print tela principal](src/print.png)

## Desenvolvimento

A ideia inicial para esse projeto seria utilizar React para o seu desenvolvimento, mas por se tratar de uma SPA, o melhor foi desenvolver com um html básico utilizando de tailwind e demais frameworks recentes, buscando utilizar as tecnologias que estão presentes hoje, no mercado.

Os dados de clientes foram extraídos da API: https://randomuser.me/api/?results=500

Para o desenvolvimento da tabela, foi utilizado o gridJS, e para os gráficos, a melhor escolha foi o chartJS.