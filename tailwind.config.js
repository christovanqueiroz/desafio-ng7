/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.{html,js}"],
  theme: {
    colors: {
      'gray-dark': '#273444',
      'gray-main': '#8492a6',
      'gray-light': '#d3dce6',
      'blue': '#1fb6ff',
      'green': '#12ab50',
      'white': '#fff',
      'black': '#000'
    },
    extend: {
    },
    fontFamily: {
      'sans': ['Roboto']
    }
  },
  plugins: [],
}