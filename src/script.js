import * as x from '../node_modules/chart.js/dist/chart.umd.js'
import * as y from '../node_modules/gridjs/dist/gridjs.umd.js'

const customersGenderChart = document.getElementById('customersGenderChart');
const customersAgeChart = document.getElementById('customersAgeChart');
const customersCountryChart = document.getElementById('customersCountryChart');
const femaleCustomersOverEighteenChart = document.getElementById('femaleCustomersOverEighteenChart');

const canvasA = document.getElementById('canvasA');
const canvasB = document.getElementById('canvasB');
const canvasC = document.getElementById('canvasC');
const canvasD = document.getElementById('canvasD');

let chartA;
let chartB;
let chartC;
let chartD;

const grid = document.getElementById('grid');
grid.style.visibility = 'hidden';
const gridInstance = new gridjs.Grid({
  columns: [],
  data: [],
  style: {
    th: {
      color: '#273444',
      'border': '2px solid #273444',
      'text-align': 'center'
    },
    td: {
      'text-align': 'center',
      'border': '1px solid #273444'
    }
  }
}).render(grid)

customersGenderChart.addEventListener('click',
(async function() {
  const url = "https://randomuser.me/api/?results=500";
  const response = await fetch(url);

  const dataPoints = await response.json();
  const datas = dataPoints.results;

  const customersGender = datas.map(
      function(index){
          return index.gender;
      }
  )
  
  gridInstance.updateConfig({
    columns: ["Nome", "Gênero"],
    data: 
      datas.map(customers => [`${customers.name.first} ${customers.name.last}`, customers.gender]),
    }).forceRender()
  

  grid.style.visibility = 'visible';

  chartA = new Chart(
      canvasA,
      {
        type: 'doughnut',
        data: {
          labels: [
            'Masculino',
            'Feminino'
          ],
          datasets: [
            {
              label: [
                'Clientes por gênero'
              ],
              data: [
                customersGender.filter(m => m === 'male').length,
                customersGender.filter(f => f === 'female').length
              ],
              backgroundColor: [
                'rgb(54, 162, 235)',
                'rgb(255, 99, 132)'
              ],
              hoverOffset: 4
            }
          ]
        },
      }
    );

    canvasA.style.visibility = 'visible'
    canvasB.style.visibility = 'hidden',
    canvasC.style.visibility = 'hidden',
    canvasD.style.visibility = 'hidden'

    chartB.destroy();
    chartC.destroy();
    chartD.destroy();
  }
)
)

customersAgeChart.addEventListener('click', 
(async function() {
  const url = "https://randomuser.me/api/?results=500";
  const response = await fetch(url);

  const dataPoints = await response.json();
  const datas = dataPoints.results;

  const customersAge = datas.map(
      function(index){
          return index.dob.age;
      }
  )

  function alcance(value) {
    return value >= this.menor && value <= this.maior;
  }

  let azul = {
    menor: 18,
    maior: 30
  }
  
  let vermelho = {
    menor: 31,
    maior: 45
  }

  let amarelo = {
    menor: 46,
    maior: 60
  }

  let verde = {
    menor: 61,
    maior: 150
  }

  gridInstance.updateConfig({
    columns: ["Nome", "Idade"],
    data: 
      datas.map(customers => [`${customers.name.first} ${customers.name.last}`, customers.dob.age]),
  }).forceRender()
  

  grid.style.visibility = 'visible';

  chartB = new Chart(
    canvasB,
    {
      type: 'pie',
      data: {
        labels: [
          '18 - 30 anos',
          '31 - 45 anos',
          '46 - 60 anos',
          'Acima de 61 anos'
        ],
        datasets: [
          {
            label: [
              'Clientes por idade'
            ],
            data: [
              customersAge.filter(alcance,azul).length,
              customersAge.filter(alcance,vermelho).length,
              customersAge.filter(alcance,amarelo).length,
              customersAge.filter(alcance,verde).length
            ],
            backgroundColor: [
              'rgb(54, 162, 235)',
              'rgb(255, 99, 132)',
              'rgb(254, 186, 5)',
              'rgb(94, 162, 15)'
            ],
            hoverOffset: 4
          }
        ]
      }
    }
  );

  canvasA.style.visibility = 'hidden',
  canvasB.style.visibility = 'visible',
  canvasC.style.visibility = 'hidden',
  canvasD.style.visibility = 'hidden'

  chartA.destroy();
  chartC.destroy();
  chartD.destroy();
}
)
)

customersCountryChart.addEventListener('click',
(async function() {
  const url = "https://randomuser.me/api/?results=500";
  const response = await fetch(url);

  const dataPoints = await response.json();
  const datas = dataPoints.results;

  const customersCountry = datas.map(
    function(index){
      return index.location.country;
    }
  )

  let resultado = customersCountry.reduce((acc, val) => {
    if (!acc[val]) acc[val] = {
      "país": val,
      "quantidade": 1
    };
    else acc[val]["quantidade"]++;
    return acc;
  }, {});

  const countries = (Object.values(resultado).map(country => country.país));
  const customersByCountry = (Object.values(resultado).map(country => country.quantidade));

  gridInstance.updateConfig({
    columns: ["Nome", "País"],
    data: 
      datas.map(customers => [`${customers.name.first} ${customers.name.last}`, customers.location.country]),
  }).forceRender()
  
  grid.style.visibility = 'visible';

  chartC = new Chart(
    canvasC,
    {
      type: 'bar',
      data: {
        labels: 
          countries
        ,
        datasets: [
          {
            label: [
              'Clientes por país'
            ],
            data: 
              customersByCountry
            ,
            backgroundColor: [
              'rgb(54, 162, 235)',
              'rgb(255, 99, 132)',
              'rgb(254, 186, 5)',
              'rgb(94, 162, 15)'
            ],
            hoverOffset: 4,
            borderWidth: 1,
          }
        ]
      }
    }
  );

  canvasA.style.visibility = 'hidden',
  canvasB.style.visibility = 'hidden',
  canvasC.style.visibility = 'visible',
  canvasD.style.visibility = 'hidden'

  chartA.destroy();
  chartB.destroy();
  chartD.destroy();
}
)
)

femaleCustomersOverEighteenChart.addEventListener('click',
(async function() {
  const url = "https://randomuser.me/api/?results=500";
  const response = await fetch(url);

  const dataPoints = await response.json();
  const datas = dataPoints.results;

  const customers = datas.map(
    function(index){
      return index;
    }
  )

  const femaleCustomers = customers.filter(fc => fc.gender === 'female');
  const overEighteen = femaleCustomers.filter(oe => oe.dob.age >= 18);

  gridInstance.updateConfig({
    columns: ["Nome", "Idade"],
    data: 
      datas.filter(fc => fc.gender === 'female').map(customers => [`${customers.name.first} ${customers.name.last}`, customers.dob.age]),
  }).forceRender()
  
  grid.style.visibility = 'visible';

  chartD = new Chart(
    canvasD,
    {
      type: 'doughnut',
      data: {
        labels: [
          'Clientes mulheres',
          'Mulheres acima de 18 anos'
        ],
        datasets: [
          {
            label: [
              'Clientes mulheres acima de 18 anos'
            ],
            data: [
              femaleCustomers.length,
              overEighteen.length
            ],
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(94, 162, 15)'
            ],
            hoverOffset: 4,
          }
        ]
      }
    }
  );

  canvasA.style.visibility = 'hidden',
  canvasB.style.visibility = 'hidden',
  canvasC.style.visibility = 'hidden',
  canvasD.style.visibility = 'visible'

  chartA.destroy();
  chartB.destroy();
  chartC.destroy();
})
)